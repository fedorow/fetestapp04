import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ConfigurationModel} from '@app/models/configuration-model';
import {deserialize} from '@app/models/json-member';

@Injectable()
export class ConfigurationService {

  public static CONFIG_FILE = 'assets/configuration.json';

  public get configuration(): ConfigurationModel {
    return this.config;
  }

  private config: ConfigurationModel;

  constructor(private http: HttpClient) {
  }

  public async load(): Promise<ConfigurationModel> {
    const response = await this.http
      .get(ConfigurationService.CONFIG_FILE)
      .toPromise();

    this.config = deserialize(response, ConfigurationModel);

    return this.config;
  }

}
