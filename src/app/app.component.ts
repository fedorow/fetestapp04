import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'of-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  public selected: string;

  constructor(private router: Router) {
  }

  ngOnInit(): void {
    this.selected = location.pathname.replace('\/', '');
  }

  onMenuClick() {
    this.router.navigate([this.selected]);
  }

}
