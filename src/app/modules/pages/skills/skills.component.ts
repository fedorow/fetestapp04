import {Component, OnInit} from '@angular/core';
import {ConfigurationService} from '@app/services/configuration.service';

@Component({
  selector: 'of-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss']
})
export class SkillsComponent implements OnInit {

  public skills: Array<string>;

  constructor(private configService: ConfigurationService) {
  }

  ngOnInit() {
    this.skills = this.configService.configuration.skills;
  }

}
