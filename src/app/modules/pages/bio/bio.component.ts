;
import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'of-bio',
  templateUrl: './bio.component.html',
  styleUrls: ['./bio.component.scss']
})
export class BioComponent implements OnInit {

  private readonly BIRTH_YEAR = 1985;
  private readonly BIRTH_MONTH = 1;
  private readonly BIRTH_DATE = 17;

  public get age(): number {
    const birthDay = new Date();
    birthDay.setMonth(this.BIRTH_MONTH - 1, this.BIRTH_DATE);

    let years = birthDay.getFullYear() - this.BIRTH_YEAR;
    if (birthDay > new Date()) {
      years--;
    }

    return years;
  }

  constructor() {
  }

  ngOnInit() {
  }

}
