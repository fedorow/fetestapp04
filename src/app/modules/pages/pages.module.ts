import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {ExperienceComponent} from '@app/modules/pages/experience/experience.component';
import {AdditionalComponent} from '@app/modules/pages/additional/additional.component';
import {SkillsComponent} from '@app/modules/pages/skills/skills.component';
import {ContactsComponent} from '@app/modules/pages/contacts/contacts.component';
import {BioComponent} from '@app/modules/pages/bio/bio.component';
import {NotFoundComponent} from '@app/modules/pages/not-found/not-found.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [
    NotFoundComponent,
    AdditionalComponent,
    ExperienceComponent,
    SkillsComponent,
    ContactsComponent,
    BioComponent
  ]
})
export class PagesModule {
}
