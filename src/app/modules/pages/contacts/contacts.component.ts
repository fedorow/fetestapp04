import {Component, OnInit} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import {ConfigurationService} from '@app/services/configuration.service';
import {ContactsModel} from '@app/models/contacts-model';

@Component({
  selector: 'of-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss']
})
export class ContactsComponent implements OnInit {

  public contacts: ContactsModel;

  constructor(private configService: ConfigurationService,
              private sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    this.contacts = this.configService.configuration.contacts;
  }

  getEmailLink() {
    return this.sanitizer.bypassSecurityTrustUrl(`mailto:${this.contacts.email}`);
  }

  getSkypeLink() {
    return this.sanitizer.bypassSecurityTrustUrl(`callto:${this.contacts.skype}`);
  }

}
