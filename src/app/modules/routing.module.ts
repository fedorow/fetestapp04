import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {BioComponent} from '@app/modules/pages/bio/bio.component';
import {ContactsComponent} from '@app/modules/pages/contacts/contacts.component';
import {SkillsComponent} from '@app/modules/pages/skills/skills.component';
import {ExperienceComponent} from '@app/modules/pages/experience/experience.component';
import {AdditionalComponent} from '@app/modules/pages/additional/additional.component';
import {NotFoundComponent} from '@app/modules/pages/not-found/not-found.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'bio'},
  {path: 'bio', component: BioComponent},
  {path: 'contacts', component: ContactsComponent},
  {path: 'skills', component: SkillsComponent},
  {path: 'experience', component: ExperienceComponent},
  {path: 'additional', component: AdditionalComponent},
  {path: '**', component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class RoutingModule {
}
