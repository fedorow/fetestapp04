import {BrowserModule} from '@angular/platform-browser';
import {APP_INITIALIZER, NgModule} from '@angular/core';
import {AppComponent} from '@app/app.component';
import {RoutingModule} from '@app/modules/routing.module';
import {ButtonsModule} from 'ngx-bootstrap';
import {FormsModule} from '@angular/forms';
import {PagesModule} from '@app/modules/pages/pages.module';
import {ConfigurationService} from '@app/services/configuration.service';
import {HttpClientModule} from '@angular/common/http';


export function ConfigLoaderFactory(configurationService: ConfigurationService) {
  return () => configurationService.load();
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    RoutingModule,
    FormsModule,
    PagesModule,
    ButtonsModule.forRoot(),
    HttpClientModule
  ],
  providers: [
    ConfigurationService,
    {
      provide: APP_INITIALIZER,
      useFactory: ConfigLoaderFactory,
      deps: [ConfigurationService],
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
