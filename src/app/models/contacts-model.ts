import {JsonMember} from '@app/models/json-member';

export class ContactsModel {

  @JsonMember
  public phones: Array<string>;

  @JsonMember
  public email: string;

  @JsonMember
  public skype: string;

  @JsonMember
  public telegram: string;

  @JsonMember
  public linkedin: string;

}
