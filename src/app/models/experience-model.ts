import {JsonMember} from '@app/models/json-member';

export class ExperienceModel {

  @JsonMember
  public title: string;

  @JsonMember
  public position: string;

  @JsonMember
  public description: string;

}
