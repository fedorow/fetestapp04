import {JsonMember} from '@app/models/json-member';
import {ExperienceModel} from '@app/models/experience-model';
import {ContactsModel} from '@app/models/contacts-model';

export class ConfigurationModel {

  @JsonMember
  public skills: Array<string>;

  @JsonMember
  public experience: Array<ExperienceModel>;

  @JsonMember
  public contacts: ContactsModel;

}
