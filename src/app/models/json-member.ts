export function JsonMember(target: any, propertyKey: string) {
  const metadataFieldKey = '__propertyTypes__';
  const propertyTypes = target[metadataFieldKey] || (target[metadataFieldKey] = {});
  propertyTypes[propertyKey] = Reflect['getMetadata']('design:type', target, propertyKey);
}

export function deserialize<T>(jsonObject: any, Constructor: { new(): T }): T {
  if (!Constructor || !Constructor.prototype.__propertyTypes__ || !jsonObject || typeof jsonObject !== 'object') {
    return jsonObject;
  }

  const instance: any = new Constructor();
  Object.keys(Constructor.prototype.__propertyTypes__).forEach(propertyKey => {
    const PropertyType = Constructor.prototype.__propertyTypes__[propertyKey];
    instance[propertyKey] = deserialize(jsonObject[propertyKey], PropertyType);
  });

  return instance;
}
